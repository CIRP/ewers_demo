# EWERS Demo
## Introduction
Provides a simple set of examples for illustrating the capabilities of mobile applications to Fishers and other fisheries stakeholders.

## Technologies
### mFisheries Core
Using the mFisheries Core Platform to provide a reusable menu

core:
* BaseActivity
* Module
* ModuleActivity
* ModuleAdapter
* ModuleFactory
* ModuleInfo

util:
* ModuleUtil
* SquareImageView

res->layout:
* layout_module.xml

res->values:
* colors.xml
* ids.xml
* strings.xml


