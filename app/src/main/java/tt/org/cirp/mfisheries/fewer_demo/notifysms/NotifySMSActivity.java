package tt.org.cirp.mfisheries.fewer_demo.notifysms;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyle on 2/13/17.
 */

public class NotifySMSActivity extends ModuleActivity {
    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_notify_sms;
    }
}