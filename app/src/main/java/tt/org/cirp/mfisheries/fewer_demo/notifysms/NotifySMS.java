package tt.org.cirp.mfisheries.fewer_demo.notifysms;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyle on 2/13/17.
 */

public class NotifySMS extends Module {
    public NotifySMS(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "SMS";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "SMS Notification";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.sms;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = NotifySMSActivity.class;
        return this;
    }
}
