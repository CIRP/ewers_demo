package tt.org.cirp.mfisheries.fewer_demo.cap;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyle on 2/13/17.
 */

public class Cap extends Module {

    public Cap(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "cap";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "CAPs";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.cap;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = CapActivity.class;
        return this;
    }
}
