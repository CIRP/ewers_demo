package tt.org.cirp.mfisheries.fewer_demo.shelter;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Random;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;
import tt.org.cirp.mfisheries.fewer_demo.model.Shelter;
import tt.org.cirp.mfisheries.fewer_demo.util.FEWERLocationManager;
import tt.org.cirp.mfisheries.fewer_demo.util.LocationResponder;

/**
 * https://developer.android.com/training/material/lists-cards.html
 * https://www.raywenderlich.com/103367/material-design
 */

public class ShelterActivity extends ModuleActivity implements LocationResponder {
    private static final String TAG = "ShelterActivity";
    private JSONObject sheltersGeoJSON;
    private JSONObject portGeoJSON;
    private RecyclerView shelterList;
    private ArrayList<Shelter> shelters;
    private ShelterAdapter shelterAdapter;
    private FEWERLocationManager fewerLocationManager;
    private TextToSpeech tts;
    private ImageButton btnSpeak;

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_shelters;
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpSpeech();
//        loadDominicaShelterGeoData();
//        loadDominicaFishingPortGeoData();
        loadStLuciaShelterData();
//        loadSVGShelterData();

        loadRecyclerView();
        loadLocation();
    }

//    private void loadSVGShelterData() {
//        loadShelterFromFileTextFile(R.raw.svg_shelter);
//    }

    private void setUpSpeech() {
        btnSpeak = (ImageButton) findViewById(R.id.talkbtn);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                talkHelp();
            }
        });

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("ShelterAct-TTS", "This Language is not supported");
                    } else {
                        btnSpeak.setEnabled(true);
                    }
                } else {
                    Log.e("ShelterAct-TTS", "Initialization Failed!");
                }
            }
        });

    }

    private void loadLocation() {
        fewerLocationManager = new FEWERLocationManager(this, this);
    }

    private void loadRecyclerView() {
        shelterList = (RecyclerView) findViewById(R.id.shelters);
        shelterList.setHasFixedSize(true);
        shelterList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        shelterAdapter = new ShelterAdapter(shelters, new ShelterOnItemClickListener() {
            @Override
            public void onItemClick(Shelter shelter) {
                displayAlertForShelter(shelter);
            }
        });
        shelterList.setAdapter(shelterAdapter);
    }

    private void loadDominicaShelterGeoData() {
        InputStream inputStream = getResources().openRawResource(R.raw.dominica_shelters);
        shelters = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine();
            String jsonStr = "";
            while (line != null) {
                jsonStr += line;
                line = reader.readLine();
            }
            sheltersGeoJSON = new JSONObject(jsonStr);
            Log.d(TAG, "Found: " + sheltersGeoJSON.length());

            // Retrieve features from the JSON
            JSONArray features = sheltersGeoJSON.getJSONArray("features");
            Log.d(TAG, "Features size was: " + features.length());
            for (int i = 0; i < features.length(); i += 1) {
                JSONObject rec = features.getJSONObject(i);
                Log.d(TAG, rec.toString());
                try {
                    String name = rec.getJSONObject("properties").getString("Name");
                    double lat, lon;
                    if (rec.getJSONObject("properties").get("Latitude") != null)
                        lat = rec.getJSONObject("properties").getDouble("Latitude");
                    else break;
                    if (rec.getJSONObject("properties").get("Longitude") != null)
                        lon = rec.getJSONObject("properties").getDouble("Longitude");
                    else break;

                    Random ran = new Random();
                    int rat = ran.nextInt(6) + 5;
                    Shelter shelter = new Shelter(name, lat, lon, rat);
                    shelters.add(shelter);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
            Log.d(TAG, "Received: " + shelters.size() + " records");

        } catch (java.io.IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadDominicaFishingPortGeoData() {
        InputStream inputStream = getResources().openRawResource(R.raw.dominica_fishing_ports);
        ArrayList<Shelter> shelters = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine();
            String jsonStr = "";
            while (line != null) {
                jsonStr += line;
                line = reader.readLine();
            }
            portGeoJSON = new JSONObject(jsonStr);
            Log.d(TAG, "Found: " + portGeoJSON.length());

        } catch (java.io.IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadStLuciaShelterData() {
        loadShelterFromFileTextFile(R.raw.stlucia_shelter);
    }

    private void loadShelterFromFileTextFile(int resource){
        InputStream inputStream = getResources().openRawResource(resource);
        shelters = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine();
            double lat = 0, lon = 0;
            while (line != null) {
                if (line.length() > 3) {
                    String name = "";
//                    Log.d(TAG, "Read: " + line);
                    String[] tokens = line.split("\\[");
//                    Log.d(TAG, "Tokens has : " + tokens.length + " items");
                    if (tokens.length > 1) { // found a location get lat and long
                        String[] location = tokens[1].split(",");
                        lat = Double.parseDouble(location[0]);
                        lon = Double.parseDouble(location[1]);
                    } else {
                        name = line;
                    }
                    if (name.length() > 2) {
                        Random ran = new Random();
                        int rat = ran.nextInt(6) + 5;
                        Shelter shelter = new Shelter(name, lat, lon, rat);
                        shelters.add(shelter);
                    }

                }
                line = reader.readLine();
            }
            Log.d(TAG, "Received: " + shelters.size() + " records");
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }


    private void displayAlertForShelter(Shelter shelter) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Shelter Details");

        // Setting Dialog Message
        alertDialog.setMessage("Shelter has a Rating of "
                + shelter.rating.get() +
                ". This rating is based on user review of the quality of the layout_shelter_item." +
                "Do you wish to View this Shelter on a Map");

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.star_flat_icon);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ShelterActivity.this, NavigationActivity.class);
                startActivity(i);
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public void talkHelp() {
        String textHelp = ((TextView) findViewById(R.id.txtHelpContent)).getText().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (tts != null) tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {

            if (tts != null) {
                //noinspection deprecation
                tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    @Override
    public void respondToLocationChange(final Location location) {
        Log.d(TAG, "Found Location at (" + location.getLatitude() + "," + location.getLongitude() + ")");
        // Only need one location
        if (fewerLocationManager != null) fewerLocationManager.detach();

        Collections.sort(shelters, new Comparator<Shelter>() {
            @Override
            public int compare(Shelter s1, Shelter s2) {
                Location s1Loc = s1.getLocation();
                Location s2Loc = s2.getLocation();
                return Float.compare(location.distanceTo(s1Loc), location.distanceTo(s2Loc));
            }
        });

        shelterAdapter.notifyDataSetChanged();
    }

    @Override
    public void respondToIsAttached(boolean attachedStatus) {

    }
}
