package tt.org.cirp.mfisheries.fewer_demo.preparation;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class Preparation extends Module {

    public Preparation(Context context) {
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "prepare";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Preparation";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.prepare;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = Preparation.class;
        return this;
    }
}
