package tt.org.cirp.mfisheries.fewer_demo.mapspoint;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyledefreitas on 2/16/17.
 */

public class MapsPointListActivity extends ModuleActivity {

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView view = (ListView) findViewById(R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.centreTitle));
        view.setAdapter(adapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                handleItemSelected(i);
            }
        });
    }

    private void handleItemSelected(int position) {
        String[] comments = getResources().getStringArray(R.array.centreComment);
        String[] titles = getResources().getStringArray(R.array.centreTitle);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Emergency Locations")
                .setMessage(String.format("%s: %s ", titles[position], comments[position]))
                .setPositiveButton("Navigate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("google.navigation:q=St+Augustine,Trinidad+and+Tobago"));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("View", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(MapsPointListActivity.this, MapsPointActivity.class);
                        startActivity(intent);
                    }
                })
                .create()
                .show();
    }
}
