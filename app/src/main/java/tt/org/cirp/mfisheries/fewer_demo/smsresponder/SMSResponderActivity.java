package tt.org.cirp.mfisheries.fewer_demo.smsresponder;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import tt.org.cirp.mfisheries.core.BaseActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class SMSResponderActivity extends BaseActivity {

    private static final String TAG = "SMSResponder";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_smsresponder);

        String content = null;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                Log.d(TAG, "Using extra received through intent");
                content = extras.getString("sms_message");
                Log.d(TAG, content);
                updateContent(content);
            } else {
                Log.d(TAG, "Using default");
                content = "Alert:Emergency:Storm Surge on Coast and Open Waters:";
                updateContent(content);
            }
        }
    }

    protected void updateContent(String smsMsg) {
        String[] components = smsMsg.split(":");
        if (components.length > 2) {
            String alert_type = components[1];
            String alert_title = components[2];

            if (alert_type.equalsIgnoreCase("warning")) {
                Log.d(TAG, "Detected as Warning");
                findViewById(R.id.page).setBackgroundColor(ContextCompat.getColor(this, R.color.yellowDark));
            }

            ((TextView) findViewById(R.id.alert_type)).setText(String.format("%s Alert", alert_type));
            ((TextView) findViewById(R.id.alert_title)).setText(String.format("%s Alert", alert_title));

            fetchContent(alert_type.toLowerCase(), alert_title.replaceAll("\\s", ""));
        }


    }

    private void fetchContent(String type, String title) {
        final ImageView imageView = (ImageView) findViewById(R.id.alert_img);
        final TextView recommendsView = (TextView) findViewById(R.id.alert_recommends);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(type);
        reference.keepSynced(true);


    }

}
