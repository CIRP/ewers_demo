package tt.org.cirp.mfisheries.fewer_demo.util;

import android.location.Location;

public interface LocationResponder {
    void respondToLocationChange(Location location);

    void respondToIsAttached(boolean attachedStatus);
}
