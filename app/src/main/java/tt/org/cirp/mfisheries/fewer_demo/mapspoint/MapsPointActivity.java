package tt.org.cirp.mfisheries.fewer_demo.mapspoint;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;


public class MapsPointActivity extends ModuleActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "MapsPoints";
    private final int LOCATION_PERMISSION_ID = 123;
    private GoogleApiClient mGoogleApiClient;
    private boolean isGoogleReady;
    private Location mLastLocation;
    private boolean isMapReady;
    private GoogleMap googleMap;
    private Location shelter;


    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_maps;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isGoogleReady = false;
        isMapReady = false;

        // Set Location of layout_shelter_item
        //https://www.google.tt/maps/place/Shalom+House/@10.6393248,-61.4276987,15z/data=!4m5!3m4!1s0x0:0x8b7b925f564b535d!8m2!3d10.6393248!4d-61.418944?hl=en
        shelter = new Location("");
        shelter.setLatitude(10.6393248);
        shelter.setLongitude(-61.4276987);

        // request map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // request location by connecting to location service
        if (checkPermissions()){
            connectGoogleAPI();
        }
    }

    private boolean checkPermissions() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat
                .requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    LOCATION_PERMISSION_ID
                );
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_ID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Restart Activity
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }else{
                    displayMessage("Unable to Find Location. Permission Required");
                    finish(); // Close
                }
            }

        }
    }

    private void displayMessage(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Emergency Locations")
                .setMessage(message)
                .create()
                .show();
    }

    private void displayMessage(String message, String positive){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Emergency Locations")
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create()
                .show();
    }

    public void connectGoogleAPI() {
        Log.d(TAG, "Connecting to Google API");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }


    public void displayContent(){
        CameraPosition googlePlex = CameraPosition.builder()
                .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                .zoom(16)
                .bearing(0)
                .build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(googlePlex));

        float distance = mLastLocation.distanceTo(shelter);
        String message = "The Shelters 'Shalom House' is located " + distance + " from You";
        displayMessage(message, "navigate");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        this.googleMap.setContentDescription("Area Affected By Tidal Wave");

        isMapReady = true;
        // Google Ready before Map
        if (isGoogleReady){
            displayContent();
        }else{
            Log.d(TAG, "Google Not ready when map is loaded");
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Connected to Google API");
        isGoogleReady = true;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Log.d(TAG, "Found Location to be: (" + mLastLocation.getLatitude() +","+ mLastLocation.getLongitude()+")");
            // Map Ready before Google
            if (isMapReady){
                displayContent();
            }else{
                Log.d(TAG, "Map Was not ready when location was found");
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Unable to Connect to Google API");
    }
}
