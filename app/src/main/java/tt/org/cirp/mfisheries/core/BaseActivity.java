package tt.org.cirp.mfisheries.core;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import tt.org.cirp.mfisheries.fewer_demo.R;


public abstract class BaseActivity extends AppCompatActivity {

	protected final int PERMISSIONS_REQUEST = 0x4;
	public boolean PERMISSIONS_GRANTED;
    protected String[] permissions;

	public void requestPermissions(){
		if(permissions.length > 0) {
			for(String permission : permissions) {
				if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
					if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
						explainPermission();
					} else {
						ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST);
					}
				} else {
					Log.d("Permission", "Already granted " + permission);
					onPermissionGranted(permission);
				}
			}
		}
	}

	public void requestPermission(String permission){
		if(permissions.length > 0) {
			if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
				if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
					explainPermission();
				} else {
					ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST);
				}
			} else {
				Log.d("Permission", "already granted");
				onPermissionGranted("");
			}
		}
	}

	public void explainPermission(){
		new AlertDialog.Builder(this)
				.setTitle(getString(R.string.why_need))
				.setMessage(getString(R.string.explain))
				.setPositiveButton(getString(R.string.continue_text), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						ActivityCompat.requestPermissions(BaseActivity.this, permissions, PERMISSIONS_REQUEST);
						dialog.dismiss();
					}
				})
				.show();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case PERMISSIONS_REQUEST: {
				for (int grantResult : grantResults)
					Log.d("Permission", "Result: " + grantResult);
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d("Permission", "Permission Granted " + this.getLocalClassName());
					onPermissionGranted("");
				} else if(grantResults.length != 0){
					Log.d("Permission", "Permission Rejected " + this.getLocalClassName());
					Toast.makeText(this, "Permission Rejected", Toast.LENGTH_SHORT).show();
					onPermissionRejected();
				}
				break;
			}
		}
	}

	public void onPermissionGranted(String permission) {
		PERMISSIONS_GRANTED = true;
	}

	public void onPermissionRejected() {
		PERMISSIONS_GRANTED = false;
	}

}
