package tt.org.cirp.mfisheries.fewer_demo.mapslayered;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.kml.KmlLayer;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class MapsLayeredActivity extends ModuleActivity implements OnMapReadyCallback{
    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_maps_gen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //https://www.google.tt/maps/@15.4554831,-61.3680192,12z?hl=en
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        googleMap.setContentDescription("Shelter and Flood Area Relationship");

        CameraPosition dominica = CameraPosition.builder()
                .target(new LatLng(15.4554831, -61.5005418))
                .zoom(10)
                .bearing(0)
                .build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(dominica));

        try {
            GeoJsonLayer centerLayer = new GeoJsonLayer(googleMap, R.raw.dominica_shelters, getApplicationContext());
            centerLayer.addLayerToMap();

            GeoJsonLayer fishing_ports = new GeoJsonLayer(googleMap, R.raw.dominica_fishing_ports, this);
//            fishing_ports.addLayerToMap();

            KmlLayer flashFloodLayer = new KmlLayer(googleMap, R.raw.dominica_flash_flood, this);
            flashFloodLayer.addLayerToMap();

            GeoJsonLayer coralLayer = new GeoJsonLayer(googleMap, R.raw.dominica_coral, this);
            coralLayer.addLayerToMap();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }
}
