package tt.org.cirp.mfisheries.fewer_demo.notifycloud;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;


public class NotifyCloud extends Module {

    public NotifyCloud(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "FCM";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Cloud Notification";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.cloud;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = NotifyCloudActivity.class;
        return this;
    }


}
