package tt.org.cirp.mfisheries.fewer_demo.model;

import android.databinding.ObservableField;

/**
 * Created by kyledefreitas on 2/13/17.
 */

public class Message {


    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> message = new ObservableField<>();

    public Message(String title, String message) {
        this.title.set(title);
        this.message.set(message);
    }
}
