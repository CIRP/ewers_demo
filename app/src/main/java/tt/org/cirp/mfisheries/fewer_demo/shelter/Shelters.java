package tt.org.cirp.mfisheries.fewer_demo.shelter;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyle on 3/3/17.
 */

public class Shelters extends Module {

    public Shelters(Context context) {
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "Shelters";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Shelters";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.shelter;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = ShelterActivity.class;
        return this;
    }
}
