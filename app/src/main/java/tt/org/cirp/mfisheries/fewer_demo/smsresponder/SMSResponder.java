package tt.org.cirp.mfisheries.fewer_demo.smsresponder;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class SMSResponder extends Module {
    public SMSResponder(Context context) {
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "SMSResponder";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "SMS Responder";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.alert;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = SMSResponderEnableActivity.class;
        return this;
    }
}
