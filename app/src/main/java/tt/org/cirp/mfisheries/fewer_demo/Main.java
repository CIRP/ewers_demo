package tt.org.cirp.mfisheries.fewer_demo;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import tt.org.cirp.mfisheries.core.ModuleHandler;
import tt.org.cirp.mfisheries.fewer_demo.notifycloud.User;

public class Main extends AppCompatActivity {

    private static final int MAIN_PERMISSION_REQUEST = 256;
    private static final String TAG = "EWERSMain";
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SharedPreferences instantiation
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Startup Firebase configuration
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        startUpFirebase();

        //Load the Modules
        ModuleHandler handler = new ModuleHandler(this, preferences);
        List<String> modules = handler.loadModules();
        GridView gridView = (GridView) this.findViewById(R.id.gridView);
        handler.displayModules(modules, gridView);

        requestPermissions();
    }


    public void startUpFirebase() {
        try {
            // Register The Firebase's Offline Capability
            FirebaseDatabase.getInstance().setPersistenceEnabled(true); //TODO Can cause system to crash see following for implementation guidance -> http://stackoverflow.com/questions/37448186/setpersistenceenabledtrue-crashes-app
            // Setup listener for registration change
            auth = FirebaseAuth.getInstance();
            authListener = new FirebaseAuth.AuthStateListener() {
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = auth.getCurrentUser();
                    if (user != null) {
                        Toast.makeText(Main.this, "System signed in to online service", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Main.this, "Unable to sign in to online service", Toast.LENGTH_SHORT).show();
                    }
                }
            };
            // Attempt anonymous login
            auth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                public void onComplete(@NonNull Task<AuthResult> task) {
                    Log.d(TAG, "Anonymous Sign in was successful: " + task.isSuccessful());
                    String userId = "12";
                    User user = new User(userId, "Trinidad");
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                    mDatabase.child("users").child(userId).setValue(user);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (auth != null)
            auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null && auth != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void changeCountry(View view) {
        Toast.makeText(this, "Selected Option to Change Country", Toast.LENGTH_SHORT).show();
    }


    // Requesting for All permissions needed by application for background services
    private void requestPermissions() {
        ArrayList<String> permissionList = new ArrayList<>();
        String[] neededPermission = new String[]{
                android.Manifest.permission.RECEIVE_SMS,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        for (String perm : neededPermission) {
            if (ActivityCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(perm);
            }
        }

        if (permissionList.size() > 0) {
            // Convert to String array needed by Request Permission method
            String[] permissions = new String[permissionList.size()];
            permissions = permissionList.toArray(permissions);

            // Make Request for All Permissions Needed
            ActivityCompat
                    .requestPermissions(
                            this,
                            permissions,
                            MAIN_PERMISSION_REQUEST
                    );
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MAIN_PERMISSION_REQUEST: {
                for (int res = 0; res < grantResults.length; res += 1) {
                    String permission = permissions[res];
                    if (grantResults[res] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Permission For " + permission + " was granted");
                        Toast.makeText(this, "Permission For " + permission + " was granted", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d(TAG, "Permission For " + permission + " was denied");
                        Toast.makeText(this, "Permission For " + permission + " was denied", Toast.LENGTH_SHORT).show();
                    }
                }

                // Restart Activity
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
