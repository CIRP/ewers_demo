package tt.org.cirp.mfisheries.fewer_demo.mapsarea;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class MapsArea extends Module {
    public MapsArea(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "MapsArea";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Maps Area";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.area;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = MapsAreaActivity.class;
        return this;
    }
}
