package tt.org.cirp.mfisheries.fewer_demo.mapspoint;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class MapsPoint extends Module  {
    public MapsPoint(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "Points";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Map Points";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.point;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = MapsPointListActivity.class;
        return this;
    }
}
