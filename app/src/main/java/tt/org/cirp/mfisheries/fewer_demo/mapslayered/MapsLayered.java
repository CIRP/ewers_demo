package tt.org.cirp.mfisheries.fewer_demo.mapslayered;

import android.content.Context;

import tt.org.cirp.mfisheries.core.Module;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyle on 2/13/17.
 */

public class MapsLayered extends Module {
    public MapsLayered(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "Layered";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Maps Layered";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.layer;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = MapsLayeredActivity.class;
        return this;
    }
}
