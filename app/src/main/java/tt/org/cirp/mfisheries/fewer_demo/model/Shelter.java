package tt.org.cirp.mfisheries.fewer_demo.model;

import android.databinding.ObservableField;
import android.location.Location;

/**
 * Created by kyle on 3/3/17.
 */

public class Shelter {

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<Double> latitude = new ObservableField<>();
    public ObservableField<Double> longitude = new ObservableField<>();
    public ObservableField<Integer> rating = new ObservableField<>();
    private Location location;


    public Shelter(String name, Double latitude, Double longitude, Integer rating) {
        this.name.set(name);
        this.latitude.set(latitude);
        this.longitude.set(longitude);
        this.rating.set(rating);

        this.location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
    }

    public Location getLocation() {
        return location;
    }

    public String toString() {
        return (new StringBuilder()).append("Name: ").append(name.get())
                .append("(").append(latitude.get()).append(",").append(longitude.get()).append(")")
                .toString();
    }
}
