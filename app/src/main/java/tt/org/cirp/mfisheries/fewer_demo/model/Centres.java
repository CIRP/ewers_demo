package tt.org.cirp.mfisheries.fewer_demo.model;

import android.databinding.ObservableField;


public class Centres {
    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<Double> latitude = new ObservableField<>();
    public ObservableField<Double> longitude = new ObservableField<>();
    public ObservableField<Float> distance = new ObservableField<>();
    public ObservableField<Integer> rating = new ObservableField<>();
    public ObservableField<String> comment = new ObservableField<>();


    public Centres(String title, Double latitude, Double longitude, Float distance, Integer rating, String comment) {
        this.title.set(title);
        this.latitude.set(latitude);
        this.longitude.set(longitude);
        this.distance.set(distance);
        this.rating.set(rating);
        this.comment.set(comment);
    }
}
