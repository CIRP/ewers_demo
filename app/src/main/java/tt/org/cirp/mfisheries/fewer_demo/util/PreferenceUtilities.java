package tt.org.cirp.mfisheries.fewer_demo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceUtilities {

    private static final String SMS_ENABLED = "SMS";
    private static final String COUNTRY_SELECTED = "COUNTRY";
    private final SharedPreferences preferences;


    public PreferenceUtilities(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isSMSResponderEnabled(){
        return preferences.getBoolean(SMS_ENABLED, true);
    }

    public PreferenceUtilities setSMSResponder(boolean val) {
        preferences.edit().putBoolean(SMS_ENABLED, val).apply();
        return this;
    }

    public String getCountry() {
        return preferences.getString(COUNTRY_SELECTED, "dominica");
    }

    public PreferenceUtilities setCountry(String country) {
        preferences.edit().putString(COUNTRY_SELECTED, country).apply();
        return this;
    }
}
