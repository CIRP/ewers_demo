package tt.org.cirp.mfisheries.fewer_demo.mapsarea;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Locale;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;
import tt.org.cirp.mfisheries.fewer_demo.util.FEWERLocationManager;
import tt.org.cirp.mfisheries.fewer_demo.util.LocationResponder;

public class MapsAreaActivity extends ModuleActivity implements OnMapReadyCallback, LocationResponder {

    private final String TAG = "MapsArea";
    HLCircle circleManager;
    private ImageButton btnSpeak;
    private TextToSpeech tts;
    private ProgressDialog loadingDialog;
    private FEWERLocationManager fewerLocationManager;
    private GoogleMap googleMap;
    private Location currLocation;

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_maps;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpSpeech();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        startDialog();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // @10.6400295,-61.4026951,15z?hl=en
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.setContentDescription("Area affected by sea level rise");
        this.googleMap = googleMap;

        // Start Request to Find Location
        loadLocation(); // When Location is found, then it will update the map

//         Starter Location in UWI
//        Location temp = new Location("");
//        temp.setLatitude(10.6400295);
//        temp.setLongitude(-61.4026951);
//        updateLocation(temp);
    }

    public void updateLocation(Location location) {
        currLocation = location;
        CameraPosition googlePlex = CameraPosition.builder()
                .target(new LatLng(currLocation.getLatitude(), currLocation.getLongitude()))
                .zoom(16)
                .bearing(0)
                .build();

        if (googleMap != null) {
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(googlePlex));
            if (addCircles(googleMap)) {
                googleMap.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
                    @Override
                    public void onCircleClick(Circle circle) {
                        displayCircleMessage(circle);
                    }
                });
                closeDialog();
            }
        }
    }


    private boolean addCircles(GoogleMap googleMap) {
        if (currLocation != null) {
            circleManager = new HLCircle(googleMap);
            circleManager
                    .createCircle(new LatLng(currLocation.getLatitude(), currLocation.getLongitude()), 300, "Under 5 Feet of Water", "warning")
                    .createCircle(new LatLng(currLocation.getLatitude(), currLocation.getLongitude()), 200, "Under 10 Feet of Water", "emergency");
            return true;
        } else {
            loadLocation();
            return false;
        }
    }

    private void displayCircleMessage(Circle circle) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Area affected by sea level rise")
                .setIcon(R.drawable.alert_icon)
                .setMessage(circleManager.getMessage(circle))
                .setNeutralButton("Close", null)
                .create()
                .show();
    }

    private void setUpSpeech() {
        btnSpeak = (ImageButton) findViewById(R.id.talkbtn);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                talkHelp();
            }
        });

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("ShelterAct-TTS", "This Language is not supported");
                    } else {
                        btnSpeak.setEnabled(true);
                    }
                } else {
                    Log.e("ShelterAct-TTS", "Initialization Failed!");
                }
            }
        });

    }

    public void talkHelp() {
        String textHelp = ((TextView) findViewById(R.id.txtHelpContent)).getText().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (tts != null) tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {

            if (tts != null) {
                //noinspection deprecation
                tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.hide();
        }
        super.onDestroy();
    }

    private void loadLocation() {
        if (loadingDialog != null && loadingDialog.isShowing())
            loadingDialog.setMessage("Finding Current Location");
        fewerLocationManager = new FEWERLocationManager(this, this);
        fewerLocationManager.register();
    }

    @Override
    public void respondToLocationChange(Location location) {
        Log.d(TAG, "Found Location at (" + location.getLatitude() + "," + location.getLongitude() + ")");
        if (fewerLocationManager != null) fewerLocationManager.detach();
        if (loadingDialog != null && loadingDialog.isShowing())
            loadingDialog.setMessage("Location Found ...  Sorting Location by distance");
        updateLocation(location);
    }

    @Override
    public void respondToIsAttached(boolean attachedStatus) {

    }

    public void startDialog() {
        loadingDialog = ProgressDialog.show(this, "Finding Location", "Using the Device GPS to Find Current Location", true);
        loadingDialog.show();
    }

    public void closeDialog() {
        if (loadingDialog != null && loadingDialog.isShowing())
            loadingDialog.hide();
    }

    private class HLCircle {

        private final HashMap<String, String> circleMessage;
        private final GoogleMap mMap;

        public HLCircle(GoogleMap mMap) {
            this.mMap = mMap;
            circleMessage = new HashMap<>();
        }

        public HLCircle createCircle(LatLng center, double radius, String message, String type) {
            int colour = 0;
            if (type.equalsIgnoreCase("emergency")) {
                colour = Color.argb(200, 239, 111, 111);
            } else {
                colour = Color.argb(200, 244, 241, 66);
            }
            Circle circle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radius)
                    .strokeWidth(4)
                    .strokeColor(Color.BLACK)
                    .fillColor(colour)
                    .clickable(true));

            circleMessage.put(circle.getId(), message);
            return this;
        }

        public String getMessage(Circle circle) {
            return circleMessage.get(circle.getId());
        }

    }
}
