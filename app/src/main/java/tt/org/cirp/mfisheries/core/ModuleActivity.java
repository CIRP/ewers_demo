package tt.org.cirp.mfisheries.core;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import tt.org.cirp.mfisheries.fewer_demo.R;


public abstract class ModuleActivity extends BaseActivity {

	public Module module;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResourceId());

		// Set Up Toolbar
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			//set the colour for the toolbar and status bar
			toolbar.setBackgroundColor(ContextCompat.getColor(this, getColor()));
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(ContextCompat.getColor(this, getColorDark()));
			}
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if(module != null) {
			String name = module.getName();
		}
	}

	public abstract int getLayoutResourceId();

	public int getColor(){
		return R.color.blue;
	}

	public int getColorDark(){
		return R.color.blueDark;
	}
}
