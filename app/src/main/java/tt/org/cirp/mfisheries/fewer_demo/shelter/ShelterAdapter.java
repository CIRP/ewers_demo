package tt.org.cirp.mfisheries.fewer_demo.shelter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import tt.org.cirp.mfisheries.fewer_demo.databinding.LayoutShelterItemBinding;
import tt.org.cirp.mfisheries.fewer_demo.model.Shelter;

/**
 * https://medium.com/google-developers/android-data-binding-recyclerview-db7c40d9f0e4#.bnbkunfzh
 */

public class ShelterAdapter extends RecyclerView.Adapter<ShelterViewHolder> {
    private final ArrayList<Shelter> shelters;
    ShelterOnItemClickListener mItemClickListener;


    public ShelterAdapter(ArrayList<Shelter> shelters, ShelterOnItemClickListener mItemClickListener) {
        this.shelters = shelters;
        this.mItemClickListener = mItemClickListener;
    }


    @Override
    public ShelterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        LayoutShelterItemBinding binding = LayoutShelterItemBinding.inflate(layoutInflater, parent, false);
        return new ShelterViewHolder(binding, mItemClickListener);
    }

    public void setOnItemClickListener(final ShelterOnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public void onBindViewHolder(ShelterViewHolder holder, int position) {
        Shelter shelter = shelters.get(position);
        holder.bind(shelter);
    }

    @Override
    public int getItemCount() {
        return shelters.size();
    }
}
