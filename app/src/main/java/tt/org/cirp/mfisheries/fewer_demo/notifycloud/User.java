package tt.org.cirp.mfisheries.fewer_demo.notifycloud;

/**
 * Created by kcred on 5/18/2017.
 */

public class User {

    public String userid;
    public String country;

    public User(String userid, String country) {
        this.userid = userid;
        this.country = country;
    }
}
