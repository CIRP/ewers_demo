package tt.org.cirp.mfisheries.fewer_demo.shelter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import tt.org.cirp.mfisheries.fewer_demo.databinding.LayoutShelterItemBinding;
import tt.org.cirp.mfisheries.fewer_demo.model.Shelter;

/**
 * Created by kyle on 3/3/17.
 */

public class ShelterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final LayoutShelterItemBinding binding;
    private ShelterOnItemClickListener mItemClickListener;

    public ShelterViewHolder(LayoutShelterItemBinding binding, ShelterOnItemClickListener mItemClickListener) {
        super(binding.getRoot());
        this.mItemClickListener = mItemClickListener;

        LinearLayout placeHolder = binding.mainHolder;
        placeHolder.setOnClickListener(this);
        this.binding = binding;
    }


    public void bind(Shelter shelter) {
        binding.setShelter(shelter);
        binding.executePendingBindings();
    }

    @Override
    public void onClick(View view) {
        if (mItemClickListener != null) {
            mItemClickListener.onItemClick(binding.getShelter());
        }
    }
}
