package tt.org.cirp.mfisheries.fewer_demo.shelter;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;

/**
 * Created by kyle on 3/13/17.
 */

public class NavigationActivity extends ModuleActivity {
    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_nav;
    }
}
