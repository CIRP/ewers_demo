package tt.org.cirp.mfisheries.fewer_demo.preparation;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;

public class PreparationActivity extends ModuleActivity {


    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_preparation;
    }
}
