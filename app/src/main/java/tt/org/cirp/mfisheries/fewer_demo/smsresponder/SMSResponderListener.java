package tt.org.cirp.mfisheries.fewer_demo.smsresponder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import tt.org.cirp.mfisheries.fewer_demo.util.PreferenceUtilities;

public class SMSResponderListener extends BroadcastReceiver {
    private static final String TAG = "SMSResponderListener";

    @SuppressWarnings("deprecation")
    @Override
    public void onReceive(Context context, Intent intent) {
        PreferenceUtilities pref = new PreferenceUtilities(context);

        if (pref.isSMSResponderEnabled() && intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Log.d(TAG, "Received Text Message");
            String msgBody;
            SmsMessage smsMessage = null;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) { //KITKAT
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    if (pdus != null) {
                        smsMessage = SmsMessage.createFromPdu((byte[]) pdus[0]);
                    }
                }
            } else {
                SmsMessage[] msgs;
                msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                smsMessage = msgs[0];
            }

            if (smsMessage != null) {
                msgBody = smsMessage.getMessageBody();
                Log.d(TAG, "Detected message as: " + msgBody);
                handleMessage(context, msgBody);
            }
        }
    }

    public void handleMessage(Context context, String msgBody) {
        Log.d(TAG, "Received a message of " + msgBody.length() + " characters");
        Intent i = new Intent(context, SMSResponderActivity.class);
        i.putExtra("sms_message", msgBody);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
