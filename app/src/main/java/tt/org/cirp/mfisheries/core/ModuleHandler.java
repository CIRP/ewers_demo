package tt.org.cirp.mfisheries.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import tt.org.cirp.mfisheries.core.util.ModuleUtil;


/**
 * Created by kyle on 2/13/17.
 */

public class ModuleHandler {

    private Context context;
    private SharedPreferences preferenceManager;

    public ModuleHandler(Context context, SharedPreferences preferenceManager) {
        this.context = context;
        this.preferenceManager = preferenceManager;
    }

    public ModuleHandler displayModules(List<String> modules, GridView view ){
        if (modules.size() % 2 != 0 || modules.size() == 0)
            modules.add(ModuleUtil.PLACEHOLDER_TILE);
        ModuleAdapter adapter = new ModuleAdapter(context, modules);
        // Set The action for empty block
        adapter.setPlaceholderListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadModules();
            }
        });
        view.setAdapter(adapter);
        return this;
    }

    public ArrayList<String> loadModules(){
        ModuleFactory moduleFactory = ModuleFactory.getInstance(context);
        ArrayList <String> moduleNames = new ArrayList<>(moduleFactory.getModuleIds().values());
        Log.d("ModuleHandler", "Retrieved: " + moduleNames.size() +" modules");
        return moduleNames;
    }
}
