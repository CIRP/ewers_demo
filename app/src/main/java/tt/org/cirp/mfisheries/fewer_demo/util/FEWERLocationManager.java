package tt.org.cirp.mfisheries.fewer_demo.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

public class FEWERLocationManager {
    Context context;
    LocationResponder responder;
    private LocationManager locationManager;
    private int min_distance;
    private int min_time;
    private LocationListener locationListener;

    public FEWERLocationManager(Context context, LocationResponder responder) {
        this.context = context;
        this.responder = responder;
        this.min_distance = 0;
        this.min_time = 0;
        init();
    }

    private void init() {
        // https://developer.android.com/guide/topics/location/strategies.html
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                if (responder != null) responder.respondToLocationChange(location);
            }

            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            public void onProviderEnabled(String s) {
            }

            public void onProviderDisabled(String s) {
            }
        };
        register();
    }

    public void register() {
        if (locationManager != null && locationListener != null) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, min_time, min_distance, locationListener);
            if (responder != null) responder.respondToIsAttached(true);
        } else {
            init();
        }
    }

    public void detach() {
        if (locationManager != null && locationListener != null) {
            locationManager.removeUpdates(locationListener);
            if (responder != null) responder.respondToIsAttached(false);
        }
    }

}
