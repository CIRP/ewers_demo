package tt.org.cirp.mfisheries.fewer_demo.notifycloud;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import tt.org.cirp.mfisheries.fewer_demo.Main;
import tt.org.cirp.mfisheries.fewer_demo.R;
import tt.org.cirp.mfisheries.fewer_demo.cap.CapActivity;
import tt.org.cirp.mfisheries.fewer_demo.model.Message;

/**
 * Created by kyledefreitas on 2/13/17.
 */

public class NotifyCloudMessagingService extends FirebaseMessagingService {

    private final static String TAG = "NotifyCloudService";
    private final static int notifyID = 123;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            String title = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("message");

            String type = remoteMessage.getData().get("type");
            Log.d(TAG, "Type = " + type);
            if(type.equals("cap")){
                String alertId = remoteMessage.getData().get("alertId");
                Log.d(TAG, "Alert Id = " + alertId);
                sendAck(alertId);
            }

            sendNotification(title, message);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            String title = remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getNotification().getBody();
            sendNotification(title, message);
        }
    }

    private void sendAck(String alertId) {
        String userId = "12";

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("notif-status").child(alertId).child(userId).setValue(true);
    }

    private void sendNotification(String title, String message) {
        Log.d(TAG, title + " " + message);
        Intent intent = new Intent(this, CapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notifyID, notificationBuilder.build());
    }
}
