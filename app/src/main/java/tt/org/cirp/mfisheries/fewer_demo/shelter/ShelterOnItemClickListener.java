package tt.org.cirp.mfisheries.fewer_demo.shelter;

import tt.org.cirp.mfisheries.fewer_demo.model.Shelter;

/**
 * Created by kyle on 3/3/17.
 */

interface ShelterOnItemClickListener {
    void onItemClick(Shelter view);
}
