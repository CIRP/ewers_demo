package tt.org.cirp.mfisheries.fewer_demo.smsresponder;

import android.app.NotificationManager;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Locale;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;
import tt.org.cirp.mfisheries.fewer_demo.util.PreferenceUtilities;


public class SMSResponderEnableActivity extends ModuleActivity {

    private PreferenceUtilities prefUtil;
    private ImageButton btnSpeak;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Switch switchView = (Switch)findViewById(R.id.swt_enable);
        prefUtil = new PreferenceUtilities(this);
        switchView.setChecked(prefUtil.isSMSResponderEnabled());
        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                prefUtil.setSMSResponder(b);
                Snackbar.make(switchView, "Listener changed to: " + b, Snackbar.LENGTH_SHORT).show();
            }
        });

        setUpSpeech();
    }
    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_enable;
    }

    private void setUpSpeech() {
        btnSpeak = (ImageButton) findViewById(R.id.talkbtn);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                talkHelp();
            }
        });

        if (tts == null)
            tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        int result = tts.setLanguage(Locale.US);
                        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("ShelterAct-TTS", "This Language is not supported");
                        } else {
                            btnSpeak.setEnabled(true);
                        }
                    } else {
                        Log.e("ShelterAct-TTS", "Initialization Failed!");
                    }
                }
            });

    }

    public void talkHelp() {
        String textHelp = ((TextView) findViewById(R.id.txtHelpContent)).getText().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (tts != null) tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {

            if (tts != null) {
                //noinspection deprecation
                tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    public void viewEmergencyAlert(View view) {
        String msgBody = "Alert:Emergency:Storm Surge on Coast and Open Waters:";
        Intent i = new Intent(SMSResponderEnableActivity.this, SMSResponderActivity.class);
        i.putExtra("sms_message", msgBody);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void viewWarningAlert(View view) {
        String msgBody = "Alert:Warning:Rough Seas on Coast and Open Waters:";
        Intent i = new Intent(SMSResponderEnableActivity.this, SMSResponderActivity.class);
        i.putExtra("sms_message", msgBody);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void viewWarningNotification(View view) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.alert_icon)
                .setContentTitle("FEWER - Warning Advisory")
                .setContentText("Rough Seas on Coast and Open Waters")
                .setSound(alarmSound)
                .setPriority(NotificationCompat.PRIORITY_MAX);

        int mNotificationId = 1001;
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    @Override
    public void onStop() {
        super.onStop();
        if (tts != null) tts.shutdown();
    }
}
