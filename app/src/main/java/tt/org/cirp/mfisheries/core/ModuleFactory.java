package tt.org.cirp.mfisheries.core;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import tt.org.cirp.mfisheries.fewer_demo.cap.Cap;
import tt.org.cirp.mfisheries.fewer_demo.mapsarea.MapsArea;
import tt.org.cirp.mfisheries.fewer_demo.shelter.Shelters;
import tt.org.cirp.mfisheries.fewer_demo.smsresponder.SMSResponder;

@SuppressWarnings("WeakerAccess")
public class ModuleFactory {

	private static ModuleFactory instance;
	private Map<String, Module> modules;
	private Map<String, String> moduleNames;
	private Map<String, String> moduleIds;
	private Context context;

	public ModuleFactory(Context ctx) {
		this.context = ctx; createModules();
	}

	public static ModuleFactory getInstance(Context ctx) {
		if (instance == null)
			instance = new ModuleFactory(ctx);
		return instance;
	}

	protected void createModules(){
		modules = new HashMap<>();
		moduleNames = new HashMap<>();
		moduleIds = new HashMap<>();

		//TODO Need Redesigning (Too dependent on Application - This assignment should happen outside the class)
		Module ms[] = getSystemModules();
		// Place Defined Modules in to Hashmap based on It
		for (Module m : ms) {
			modules.put(m.getId(), m);
		}
		//
		buildMaps();
	}

	private Module[] getSystemModules() {
		return new Module[]{
//				new NotifyCloud(context),
//				new NotifySMS(context),
                new MapsArea(context),
//				new MapsLayered(context),
//				new MapsPoint(context),
//				new Preparation(context),
				new SMSResponder(context),
				new Shelters(context),
                new Cap(context)
		};
	}

	private void buildMaps(){
		for (Module m : modules.values()){
			moduleNames.put(m.getId(), m.getName());
			moduleIds.put(m.getName(), m.getId());
		}
	}

	public Map<String, Module> getModules() {
		return modules;
	}

	public Map<String, String> getModuleNames() {
		return moduleNames;
	}

	public Map<String, String> getModuleIds() {
		return moduleIds;
	}

	public Module getModule(String module) {
		if (modules.containsKey(module)){
			return modules.get(module);
		}

		//Try for the Name if regular Module failed (TODO Care for infinite loop)
		if (moduleNames.containsKey(module)){
			return getModule(moduleNames.get(module)); // retrieve using id
		}

		return null;
	}

}
