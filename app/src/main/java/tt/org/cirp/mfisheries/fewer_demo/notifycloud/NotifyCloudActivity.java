package tt.org.cirp.mfisheries.fewer_demo.notifycloud;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import tt.org.cirp.mfisheries.core.ModuleActivity;
import tt.org.cirp.mfisheries.fewer_demo.R;
import tt.org.cirp.mfisheries.fewer_demo.databinding.LayoutNotifyCloudBinding;
import tt.org.cirp.mfisheries.fewer_demo.model.Message;

public class NotifyCloudActivity extends ModuleActivity {
    private Message message;

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_notify_cloud;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutNotifyCloudBinding binding = DataBindingUtil.setContentView(this, R.layout.layout_notify_cloud);
        message = new Message("", "");
        binding.setMessage(message);
    }

    public void sendMessage(View v) {
        Log.d("NotifyActivity", "Message: " + message.message.get() + " - title: " + message.title.get());
    }

}
