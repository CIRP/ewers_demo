
CARRIACOU
    Bogles Community Center
    Dover Government School
    Lauriston Mini Stadium
    Harvey Vale Community Center
    L’Esterre R.C. School
    Harvey Vale Government School

PETITE MARTINIQuE
    Petite Martinique R.C. School
    Sacred Heart R.C. Church

ST. ANDREW NORTH EAST
    Belair Government School
    Moyah S.D.A. Church
    La Poterie S.D.A. Church
    Tivoli R.C. School
    Upper St. John's Community Centre

ST. ANDREW NORTH WEST
    Morne Longue Christian Academy
    La Kabaikie New Testament Church
    Dunfermline Evangelical Church
    St. Mary’s R.C. School
    Paradise S.D.A Church
    St. Micheal R.C. School (Byelands)

 
ST. ANDREW SOUTH EAST
    Soubise S.D.A. Church
    St. Andrew's Anglican Secondary

 
ST. ANDREW SOUTH WEST
    St. Joseph Convent (Grenville)
    Holy Innocent Anglican School
    St. Andrew R.C. School
    Brich Grove New Testament Church
    Richmond The Blando S.D.A Church
    St. Mattew's R.C. School

 
ST. DAVID NORTH
    St. Joseph R.C. School (Pomme Rose)
    Crochu Multi-purpose Center
    St. David R.C. School

 
ST. DAVID SOUTH
    Westerhall Secondary School
    St. Theresa’s R.C. School
    Westerhall S.D.A. Church

 
TOWN OF ST. GEORGE
    Grenada Boys Secondary School – All Ground floors
    St. George’s S.D.A. School - Ground Floor
    St. George Anglican Junior School - Ground floor
    J.W. Feltcher Catholic Secondary School
    Presentation Brothers College - First floor of three storey Building & Ground of the storey building
    Anglican High School - Concrete roof section

     
ST. GEORGE NORTH WEST
    Cherry Hill Community Center
    Happy Hill Secondary School - All ground floor sections
    Happy Hill R.C. School – Concrete roof building & Ground floor of two storey building
    Mt. Moritz Anglican School – Ground floor
    Mt. Mortiz S.D.A
    Fontenoy Community Center
    New Hampshire Community Center
    New Hampshire Methodist Center

     
ST. GEORGE SOUTH EAST
    Mt. Airy Community Center
    Bethel Church Hall (St. Paul's)
    Laborie Community Center
    St. Paul’s Government School
    Morne Jaloux R.C School
    Marian Prototype Shelter/Multi-purpose Center

     
ST. GEORGE NORTH EAST
    Beaulieu S.D.A. Church - Ground floor
    Beaulieu R.C. School – Ground floor
    Boca Secondary School - Ground Floor
    Constantine Methodist Church
    Vendome R.C. School

     
SOUTH ST. GEORGE
    Calliste Government School - Ground floor
    Gateway Academy
    South St. George Government School
    Maranatha Seventh Day Adventist Church (Mt. Tout) - Ground floor
    St. George's Baptist Church
    Springs Open Bible Church

     
ST. JOHN
    St. Rose Modern Secondary School
    New Life Organization (NEWLO)
    St. John Christiian Secondary School
    St. John’s R.C. School - Ground floor
    Mt. Granby Seventh Day Aventist Church

     
ST. MARK
    Non Pariel Community Center
    Maran Development Resource Center

     
ST. PATRICK EAST
    Mt. Rose S.D.A. Church - Basement
    Mt. Rose R.C Church
    Snell Hall S.D.A. Church & School
    River Sallee Community Center
    River Sallee S.D.A Church
    Rose Hill Community Center
    Rose Hill S.D.A Church

     
ST. PATRICK WEST
    St. Patrick R.C. School
    Mt. Rich Pre - Primary School